import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// elementui的引入和配置  起
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import axios from 'axios'

Vue.use(ElementUI,axios);
//在任意页面中 直接使用  this.$axios  方法来直接使用axios进行请求
Vue.prototype.$axios = axios;
axios.defaults.baseURL = 'http://localhost:8080';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
